package com.runemate.web.data;

import com.runemate.web.model.*;
import com.runemate.web.requirement.*;
import com.runemate.web.transport.dynamic.*;
import com.runemate.web.util.*;
import java.util.*;
import java.util.regex.*;
import java.util.stream.*;
import lombok.*;

@Getter
@RequiredArgsConstructor
public enum HousePortal {
    RIMMINGTON(new Coordinate(2955, 3223, 0), 1),
    TAVERLY(new Coordinate(2895, 3465, 0), 2),
    POLLNIVNEACH(new Coordinate(3340, 3003, 0), 3),
    RELLEKKA(new Coordinate(2670, 3632, 0), 4),
    BRIMHAVEN(new Coordinate(2758, 3718, 0), 5),
    YANILLE(new Coordinate(2544, 3095, 0), 6),
    HOSIDIUS(new Coordinate(1743, 3518, 0), 8),
    PRIFDDINAS(new Coordinate(3239, 6075, 0), 9);

    private final Coordinate position;
    private final int houseLocationVarbit;

    public Requirement isHome() {
        return new VarbitRequirement(2187, houseLocationVarbit, IntOp.EQ);
    }

    public static List<DialogItemTransport> constructionCapeTransports() {
        return Arrays.stream(values())
            .flatMap(v -> Stream.of(v.equipmentConstructionCapePerk(), v.inventoryConstructionCapePerk()))
            .collect(Collectors.toList());
    }

    public String getName() {
        return name().substring(0, 1).toUpperCase() + name().substring(1).toLowerCase();
    }

    private DialogItemTransport equipmentConstructionCapePerk() {
        return DialogItemTransport.builder()
            .destination(position)
            .cost(5f)
            .itemName(Pattern.compile("Construct\\. cape"))
            .itemAction(Pattern.compile("Teleport"))
            .optionPattern(Pattern.compile(getName()))
            .origin(BasicItemTransport.Origin.EQUIPMENT)
            .requirement(
                Requirements.and(
                    new EquipmentRequirement(Pattern.compile("Construct\\. cape")),
                    new TeleportsEnabled(),
                    new WildernessRequirement(20)
                )
            )
            .build();
    }

    private DialogItemTransport inventoryConstructionCapePerk() {
        return DialogItemTransport.builder()
            .destination(position)
            .cost(5f)
            .itemName(Pattern.compile("Construct\\. cape"))
            .itemAction(Pattern.compile("Teleport"))
            .optionPattern(Pattern.compile(getName()))
            .origin(BasicItemTransport.Origin.INVENTORY)
            .requirement(
                Requirements.and(
                    new EquipmentRequirement(Pattern.compile("Construct\\. cape")),
                    new TeleportsEnabled(),
                    new WildernessRequirement(20)
                )
            )
            .build();
    }
}
