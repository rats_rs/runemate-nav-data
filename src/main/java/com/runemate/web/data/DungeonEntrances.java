package com.runemate.web.data;

import com.runemate.web.model.*;
import com.runemate.web.requirement.*;
import com.runemate.web.transport.fixed.*;
import java.util.*;
import java.util.regex.*;
import lombok.experimental.*;

@UtilityClass
public class DungeonEntrances {

    public Collection<FixedTransport> getAll() {
        final List<FixedTransport> transports = new ArrayList<>();

        //Rev caves entrance 1 = Rev cave teleport scroll
        transports.add(DialogGameObjectTransport.builder()
            .source(new Coordinate(3126, 3832, 0))
            .objectPosition(new Coordinate(3124, 3831, 0))
            .destination(new Coordinate(3241, 10233, 0))
            .objectName("Cavern")
            .objectAction("Enter")
            .dialogPattern(Pattern.compile("Yes.*"))
            .requirement(SkillRequirement.Skill.AGILITY.required(60))
            .build());

        //Rev caves entrance 2 = Crevice
        transports.add(DialogGameObjectTransport.builder()
            .source(new Coordinate(3069, 3740, 0))
            .objectPosition(new Coordinate(3068, 3740, 0))
            .destination(new Coordinate(3187, 10127, 0))
            .objectName("Crevice")
            .objectAction("Jump-Down")
            .dialogPattern(Pattern.compile("Yes.*"))
            .build());

        //Rev caves entrance 3
        transports.add(DialogGameObjectTransport.builder()
            .source(new Coordinate(3075, 3653, 0))
            .objectPosition(new Coordinate(3075, 3654, 0))
            .destination(new Coordinate(3197, 10056, 0))
            .objectName("Cavern")
            .objectAction("Enter")
            .dialogPattern(Pattern.compile("Yes.*"))
            .build());

        //Rev caves exit 1
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(3246, 10215, 0))
            .objectPosition(new Coordinate(3244, 10215, 0))
            .destination(new Coordinate(3124, 3806, 0))
            .objectName("Stairs")
            .objectAction("Climb-up")
            .requirement(Requirements.none())
            .build());

        //Rev caves exit 2
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(3217, 10058, 0))
            .objectPosition(new Coordinate(3218, 10058, 0))
            .destination(new Coordinate(3102, 3655, 0))
            .objectName("Stairs")
            .objectAction("Climb-up")
            .requirement(Requirements.none())
            .build());

        //Getting kicked from Lunar Isle
        transports.add(DialogGameObjectTransport.builder()
            .source(new Coordinate(2098, 3919, 0))
            .objectPosition(new Coordinate(2098, 3920, 0))
            .destination(new Coordinate(2630, 3678, 0))
            .objectName("Bank booth")
            .objectAction("Bank")
            .dialogPattern(Pattern.compile(""))
            .requirement(new DiaryRequirement(DiaryRequirement.Region.FREMENNIK, DiaryRequirement.Difficulty.ELITE).not())
            .build());
        transports.add(DialogGameObjectTransport.builder()
            .source(new Coordinate(2101, 3918, 0))
            .objectPosition(new Coordinate(2101, 3917, 0))
            .destination(new Coordinate(2630, 3678, 0))
            .objectName("Return Orb")
            .objectAction("Teleport")
            .dialogPattern(Pattern.compile("Yes.*"))
            .requirement(new DiaryRequirement(DiaryRequirement.Region.FREMENNIK, DiaryRequirement.Difficulty.ELITE))
            .build());

        //Stairs to the Nightmare
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(3728, 3302, 0))
            .objectPosition(new Coordinate(3727, 3300, 0))
            .destination(new Coordinate(3738, 9703, 1))
            .objectName("Stairs")
            .objectAction("Climb-down")
            .requirement(Requirements.none())
            .build());
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(3738, 9703, 1))
            .objectPosition(new Coordinate(3738, 9700, 1))
            .destination(new Coordinate(3728, 3302, 0))
            .objectName("Stairs")
            .objectAction("Climb-up")
            .requirement(Requirements.none())
            .build());

        //Lizardmen Settlement
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(1293, 3659, 0))
            .objectPosition(new Coordinate(1292, 3657, 0))
            .destination(new Coordinate(1292, 10057, 0))
            .objectName("Lizard dwelling")
            .objectAction("Enter")
            .build());
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(1312, 3685, 0))
            .objectPosition(new Coordinate(1312, 3686, 0))
            .destination(new Coordinate(1312, 10086, 0))
            .objectName("Lizard dwelling")
            .objectAction("Enter")
            .build());
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(1314, 3665, 0))
            .objectPosition(new Coordinate(1314, 3663, 0))
            .destination(new Coordinate(1314, 10063, 0))
            .objectName("Lizard dwelling")
            .objectAction("Enter")
            .build());
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(1329, 3670, 0))
            .objectPosition(new Coordinate(1330, 3669, 0))
            .destination(new Coordinate(1330, 10069, 0))
            .objectName("Lizard dwelling")
            .objectAction("Enter")
            .build());

        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(1294, 10078, 0))
            .objectPosition(new Coordinate(1292, 10077, 0))
            .destination(new Coordinate(1292, 3676, 0))
            .objectName("Strange hole")
            .objectAction("Jump-in")
            .build());


        // Zeah - Catacombs central entrance
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(1639, 3673, 0))
            .objectPosition(new Coordinate(1634, 3671, 0))
            .destination(new Coordinate(1666, 10050, 0))
            .objectName("Statue")
            .objectAction("Investigate")
            .build());

        // Zeah - Catacombs North-East entrance
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(1696, 3864, 0))
            .objectPosition(new Coordinate(1696, 3865, 0))
            .destination(new Coordinate(1719, 10101, 0))
            .cost(50f)
            .objectName("Hole")
            .objectAction("Enter")
            .requirement(new VarbitRequirement(5089, 1))
            .build());

        // Zeah - Catacombs South entrance
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(1469, 3653, 0))
            .objectPosition(new Coordinate(1470, 3653, 0))
            .destination(new Coordinate(1650, 9987, 0))
            .cost(50f)
            .objectName("Hole")
            .objectAction("Enter")
            .requirement(new VarbitRequirement(5088, 1))
            .build());

        // Zeah - Catacombs North-West entrance
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(1562, 3791, 0))
            .objectPosition(new Coordinate(1563, 3791, 0))
            .destination(new Coordinate(1650, 9987, 0))
            .cost(50f)
            .objectName("Hole")
            .objectAction("Enter")
            .requirement(new VarbitRequirement(5090, 1))
            .build());

        // Catacombs of Kourend - Central exit
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(1666, 10050, 0))
            .objectPosition(new Coordinate(1666, 10051, 0))
            .destination(new Coordinate(1639, 3673, 0))
            .cost(50f)
            .objectName("Vine ladder")
            .objectAction("Climb-up")
            .build());

        // Catacombs of Kourend - North-East exit
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(1719, 10101, 0))
            .objectPosition(new Coordinate(1719, 10102, 0))
            .destination(new Coordinate(1696, 3864, 0))
            .cost(50f)
            .objectName("Vine")
            .objectAction("Climb-up")
            .build());

        // Catacombs of Kourend - South exit
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(1650, 9987, 0))
            .objectPosition(new Coordinate(1650, 9986, 0))
            .destination(new Coordinate(1469, 3653, 0))
            .cost(50f)
            .objectName("Vine")
            .objectAction("Climb-up")
            .build());

        // Catacombs of Kourend - North-West exit
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(1617, 10101, 0))
            .objectPosition(new Coordinate(1617, 10102, 0))
            .destination(new Coordinate(1562, 3791, 0))
            .cost(50f)
            .objectName("Vine")
            .objectAction("Climb-up")
            .build());

        // Kraken Cave - Entrance
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(2278, 3610, 0))
            .objectPosition(new Coordinate(2277, 3611, 0))
            .destination(new Coordinate(2276, 9988, 0))
            .objectName("Cave Entrance")
            .objectAction("Enter")
            .build());

        // Kraken Cave - Exit
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(2276, 9988, 0))
            .objectPosition(new Coordinate(2276, 9987, 0))
            .destination(new Coordinate(2278, 3610, 0))
            .objectName("Cave Exit")
            .objectAction("Leave")
            .build());

        // Lighthouse house surface door
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(2509, 3635, 0))
            .objectPosition(new Coordinate(2509, 3636, 0))
            .destination(new Coordinate(2509, 3636, 0))
            .objectName("Doorway")
            .objectAction("Walk-through")
            .build());

        // Lighthouse surface iron ladder
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(2510, 3644, 0))
            .objectPosition(new Coordinate(2509, 3644, 0))
            .destination(new Coordinate(2518, 9994, 0))
            .objectName("Iron ladder")
            .objectAction("Climb")
            .build());

        // Lighthouse dungeon iron ladder
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(2518, 9994, 0))
            .objectPosition(new Coordinate(2519, 9994, 0))
            .destination(new Coordinate(2510, 3644, 0))
            .objectName("Iron ladder")
            .objectAction("Climb")
            .build());

        // Lighthouse right strange wall
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(2516, 10002, 0))
            .objectPosition(new Coordinate(2516, 10003, 0))
            .destination(new Coordinate(2516, 10003, 0))
            .objectName("Strange wall")
            .objectAction("Open")
            .build());

        // Lighthouse left strange wall
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(2513, 10003, 0))
            .objectPosition(new Coordinate(2513, 10003, 0))
            .destination(new Coordinate(2513, 10002, 0))
            .objectName("Strange wall")
            .objectAction("Open")
            .build());

        // Lighthouse ladder into combat area
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(2515, 10005, 0))
            .objectPosition(new Coordinate(2515, 10006, 0))
            .destination(new Coordinate(2515, 10008, 0))
            .objectName("Iron ladder")
            .objectAction("Climb")
            .build());

        // Lighthouse ladder from combat area
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(2515, 10008, 0))
            .objectPosition(new Coordinate(2515, 10006, 0))
            .destination(new Coordinate(2515, 10005))
            .objectName("Iron ladder")
            .objectAction("Climb")
            .build());

        // Slayer dungeon basement entrance
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(3417, 3536, 0))
            .objectPosition(new Coordinate(3417, 3535, 0))
            .destination(new Coordinate(3412, 9932, 3))
            .objectName("Ladder")
            .objectAction("Climb-down")
            .build());

        // Slayer dungeon basement exit
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(3412, 9932, 3))
            .objectPosition(new Coordinate(3412, 9931, 3))
            .destination(new Coordinate(3417, 3536, 0))
            .objectName("Ladder")
            .objectAction("Climb-up")
            .build());

        // Ancient cavern entrance
        // TODO: Identify varp/identifier for when a player has completed first stage of barbarian training
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(2511, 3514, 0))
            .objectPosition(new Coordinate(2510, 3506, 0))
            .destination(new Coordinate(1763, 5365, 1))
            .objectName("Whirlpool")
            .objectAction("Dive in")
            .requirement(new SkillRequirement(SkillRequirement.Skill.FIREMAKING, 35))
            .build());

        // Ancient cavern log exit
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(1763, 5361, 0))
            .objectPosition(new Coordinate(1761, 5362, 0))
            .destination(new Coordinate(2531, 3446, 0))
            .objectName("Aged log")
            .objectAction("Ride")
            .build());

        // Asgarnian Ice Dungeon entrance
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(3009, 3150, 0))
            .objectPosition(new Coordinate(3008, 3150, 0))
            .destination(new Coordinate(3009, 9550, 0))
            .objectName("Trapdoor")
            .objectAction("Climb-down")
            .build());

        // Asgarnian Ice Dungeon ladder exit
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(3009, 9550, 0))
            .objectPosition(new Coordinate(3008, 9550, 0))
            .destination(new Coordinate(3009, 3150, 0))
            .objectName("Ladder")
            .objectAction("Climb-up")
            .build());

        //Kourend - Chasm of Fire (Top Level)
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(1435, 3671, 0))
            .objectPosition(new Coordinate(1436, 3671, 0))
            .destination(new Coordinate(1435, 10077, 3))
            .objectName("Chasm")
            .objectAction("Enter")
            .build());

        //Chasm of Fire (Top Level) - Kourend
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(1435, 10077, 3))
            .objectPosition(new Coordinate(1435, 10078, 3))
            .destination(new Coordinate(1435, 3671, 0))
            .objectName("Rope")
            .objectAction("Climb-up")
            .build());

        // Edgeville dungeon enterance + exit.
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(3096, 3468, 0))
            .objectPosition(new Coordinate(3097, 3468, 0))
            .destination(new Coordinate(3096, 9867, 0))
            .objectName("Trapdoor")
            .objectAction("Climb-down")
            .build());
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(3096, 9867, 0))
            .objectPosition(new Coordinate(3097, 9867, 0))
            .destination(new Coordinate(3096, 3468, 0))
            .objectName("Ladder")
            .objectAction("Climb-up")
            .build());

        //Chasm of Fire lifts
        //Map<source, objectPosition>, both coordinates for the upper level
        Map<Coordinate, Coordinate> chasmLiftPositions = new HashMap<>();
        chasmLiftPositions.put(new Coordinate(1438, 10093, 3), new Coordinate(1437, 10094, 3));
        chasmLiftPositions.put(new Coordinate(1451, 10068, 3), new Coordinate(1452, 10068, 3));
        chasmLiftPositions.put(new Coordinate(1457, 10095, 2), new Coordinate(1458, 10095, 2));
        chasmLiftPositions.put(new Coordinate(1457, 10075, 2), new Coordinate(1458, 10075, 2));
        chasmLiftPositions.forEach((source, objectPosition) -> {
            transports.add(GameObjectTransport.builder()
                .source(source)
                .objectPosition(objectPosition)
                .destination(source.derive(0, 0, -1))
                .objectName("Lift")
                .objectAction("Enter")
                .build());
            transports.add(GameObjectTransport.builder()
                .source(source.derive(0, 0, -1))
                .objectPosition(objectPosition.derive(0, 0, -1))
                .destination(source)
                .objectName("Lift")
                .objectAction("Enter")
                .build());

        });

        addKaruluumDungeon(transports);
        addDesertTreasure2Transports(transports);

        //Weiss dungeons
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(2869, 3941, 0))
            .objectPosition(new Coordinate(2867, 3939, 0))
            .destination(new Coordinate(2845, 10351, 0))
            .objectAction("Descend")
            .objectName("Stairs")
            .build());
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(2845, 10351, 0))
            .objectPosition(new Coordinate(2844, 10352, 0))
            .destination(new Coordinate(2869, 3941, 0))
            .objectAction("Climb")
            .objectName("Steps")
            .build());
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(2846, 10333, 0))
            .objectPosition(new Coordinate(2845, 10332, 0))
            .destination(new Coordinate(2902, 10337, 0))
            .objectAction("Enter")
            .objectName("Cave")
            .requirement(QuestRequirement.complete("SECRETS_OF_THE_NORTH"))
            .build());
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(2902, 10337, 0))
            .objectPosition(new Coordinate(2901, 10338, 0))
            .destination(new Coordinate(2846, 10333, 0))
            .objectAction("Enter")
            .objectName("Crevice")
            .requirement(QuestRequirement.complete("SECRETS_OF_THE_NORTH"))
            .build());

        return transports;
    }

    private void addKaruluumDungeon(List<FixedTransport> transports) {

        //Surface -> Karuluum Slayer Dungeon
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(1312, 3807, 0))
            .objectPosition(new Coordinate(1311, 3807, 0))
            .destination(new Coordinate(1311, 10188, 0))
            .objectName("Elevator")
            .objectAction("Activate")
            .build());

        //Karuluum Slayer Dungeon -> Surface
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(1311, 10188, 0))
            .objectPosition(new Coordinate(1311, 10185, 0))
            .destination(new Coordinate(1312, 3807, 0))
            .objectName("Cave exit")
            .objectAction("Exit")
            .build());

        //Hydras general area
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(1311, 10214, 0))
            .objectPosition(new Coordinate(1311, 10215, 0))
            .destination(new Coordinate(1311, 10216, 0))
            .objectName("Rocks")
            .objectAction("Climb")
            .bidirectional(true)
            .build());

        //Hydras slayer-only area
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(1307, 10252, 0))
            .objectPosition(new Coordinate(1307, 10253, 0))
            .destination(new Coordinate(1307, 10257, 0))
            .objectName("Lava gap")
            .objectAction("Jump")
            .bidirectional(true)
            .build());

        //Alchemical Hydra antechamber (antechamber is instanced)
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(1351, 10250, 0))
            .objectPosition(new Coordinate(1351, 10251, 0))
            .destination(new Coordinate(1351, 10252, 0))
            .objectName("Rocks")
            .objectAction("Climb")
            .bidirectional(true)
            .build());


        //Wyrms general area
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(1303, 10206, 0))
            .objectPosition(new Coordinate(1302, 10205, 0))
            .destination(new Coordinate(1301, 10206, 0))
            .objectName("Rocks")
            .objectAction("Climb")
            .bidirectional(true)
            .build());

        //Wyrms Slayer-only area (bidirectionnal)
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(1272, 10175, 0))
            .objectPosition(new Coordinate(1269, 10171, 0))
            .destination(new Coordinate(1272, 10170, 0))
            .objectName("Lava gap")
            .objectAction("Jump")
            .bidirectional(true)
            .build());

        //Floor 0 -> Floor 1
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(1322, 10205, 0))
            .objectPosition(new Coordinate(1321, 10205, 0))
            .destination(new Coordinate(1320, 10205, 0))
            .objectName("Rocks")
            .objectAction("Climb")
            .bidirectional(true)
            .build());

        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(1329, 10205, 0))
            .objectPosition(new Coordinate(1330, 10205, 0))
            .destination(new Coordinate(1334, 10205, 1))
            .objectName("Steps")
            .objectAction("Climb")
            .build());

        //Floor 1 -> Floor 0
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(1334, 10205, 1))
            .objectPosition(new Coordinate(1330, 10205, 1))
            .destination(new Coordinate(1329, 10205, 0))
            .objectName("Steps")
            .objectAction("Climb")
            .build());

        //Drake slayer-only area
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(1329, 10238, 1))
            .objectPosition(new Coordinate(1330, 10238, 1))
            .destination(new Coordinate(1336, 10238, 1))
            .objectName("Tunnel")
            .objectAction("Crawl through")
            .bidirectional(true)
            .build());

        //Floor 1 -> Floor 2
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(1314, 10189, 1))
            .objectPosition(new Coordinate(1314, 10188, 1))
            .destination(new Coordinate(1318, 10189, 2))
            .objectName("Steps")
            .objectAction("Climb")
            .build());

        //Floor 2 -> Floor 1
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(1318, 10189, 2))
            .objectPosition(new Coordinate(1314, 10188, 2))
            .destination(new Coordinate(1314, 10189, 1))
            .objectName("Steps")
            .objectAction("Climb")
            .build());
    }


    private void addDesertTreasure2Transports(List<FixedTransport> transports) {
        //Camdozaal <-> Lassar
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(2588, 6436, 0))
            .objectPosition(new Coordinate(2587, 6436, 0))
            .destination(new Coordinate(2922, 5826, 0))
            .objectName("Rope")
            .objectAction("Climb-up")
            .build());
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(2922, 5826, 0))
            .objectPosition(new Coordinate(2923, 5826, 0))
            .destination(new Coordinate(2588, 6436, 0))
            .objectName("Rock")
            .objectAction("Climb-rope")
            .build());

        //Camdozaal <-> Surface
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(2952, 5762, 0))
            .objectPosition(new Coordinate(2951, 5761, 0))
            .destination(new Coordinate(2997, 3494, 0))
            .objectName("Ruins Exit")
            .objectAction("Exit")
            .build());
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(2997, 3494, 0))
            .objectPosition(new Coordinate(2999, 3493, 0))
            .destination(new Coordinate(2952, 5762, 0))
            .objectName("Ruins Entrance")
            .objectAction("Enter")
            .build());

        //Kourend <-> The Stranglewoods
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(1229, 3470, 0))
            .objectPosition(new Coordinate(1227, 3469, 0))
            .destination(new Coordinate(1194, 3452, 0))
            .objectName("Rowboat")
            .objectAction("Travel")
            .build());
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(1194, 3452, 0))
            .objectPosition(new Coordinate(1227, 3469, 0))
            .destination(new Coordinate(1229, 3470, 0))
            .objectName("Rowboat")
            .objectAction("Travel")
            .build());

        //The Scar <-> The Leviathan
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(2063, 6435, 0))
            .objectPosition(new Coordinate(2065, 6435, 0))
            .destination(new Coordinate(2067, 6369, 0))
            .objectName("Rowboat")
            .objectAction("Travel")
            .build());
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(2067, 6369, 0))
            .objectPosition(new Coordinate(2065, 6371, 0))
            .destination(new Coordinate(2063, 6435, 0))
            .objectName("Rowboat")
            .objectAction("Escape")
            .build());

        //The Stranglewoods teleports to lower level
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(1146, 3434, 0))
            .objectPosition(new Coordinate(1147, 3433, 0))
            .destination(new Coordinate(1149, 3445, 0))
            .objectName("Entry")
            .objectAction("Enter")
            .build());
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(1149, 3445, 0))
            .objectPosition(new Coordinate(1147, 3444, 0))
            .destination(new Coordinate(1146, 3434, 0))
            .objectName("Entry")
            .objectAction("Enter")
            .build());

        //Ghorrock dungeon <-> Lobby
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(2927, 10353, 0))
            .objectPosition(new Coordinate(2926, 10354, 0))
            .destination(new Coordinate(3039, 6417, 0))
            .objectName("Heavy Door")
            .objectAction("Enter")
            .build());
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(3039, 6417, 0))
            .objectPosition(new Coordinate(3038, 6415, 0))
            .destination(new Coordinate(2927, 10353, 0))
            .objectName("Stairs")
            .objectAction("Climb")
            .build());

        //The Scar <-> GOTR
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(2012, 6435, 0))
            .objectPosition(new Coordinate(2011, 6432, 0))
            .destination(new Coordinate(3615, 9477, 0))
            .objectName("Abyssal Rift")
            .objectAction("Enter")
            .build());

        //GOTR <-> Wizard's basement
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(3615, 9470, 0))
            .objectPosition(new Coordinate(3613, 9468, 0))
            .destination(new Coordinate(3104, 9573, 0))
            .objectName("Portal")
            .objectAction("Enter")
            .build());
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(3104, 9573, 0))
            .objectPosition(new Coordinate(3104, 9574, 0))
            .destination(new Coordinate(3615, 9470, 0))
            .objectName("Portal")
            .objectAction("Enter")
            .build());
    }

}
