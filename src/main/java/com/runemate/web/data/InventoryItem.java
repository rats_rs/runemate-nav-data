package com.runemate.web.data;

import com.runemate.web.model.*;
import com.runemate.web.requirement.*;
import com.runemate.web.transport.dynamic.*;
import java.util.*;
import java.util.regex.*;
import java.util.stream.*;
import lombok.*;

@Getter
public enum InventoryItem {

    //Standard Tabs
    VARROCK_TELEPORT_TAB(Spell.VARROCK_TELEPORT.getDestination(), "Varrock teleport", "Break"),
    LUMBRIDGE_TELEPORT_TAB(Spell.LUMBRIDGE_TELEPORT.getDestination(), "Lumbridge teleport", "Break"),
    FALADOR_TELEPORT_TAB(Spell.FALADOR_TELEPORT.getDestination(), "Falador teleport", "Break"),
    CAMELOT_TELEPORT_TAB(Spell.CAMELOT_TELEPORT.getDestination(), "Camelot teleport", "Break"),
    ARDOUGNE_TELEPORT_TAB(Spell.ARDOUGNE_TELEPORT.getDestination(), "Ardougne teleport", "Break"),
    KOUREND_TELEPORT_TAB(Spell.KOUREND_CASTLE_TELEPORT.getDestination(), "Kourend teleport", "Break"),

    //POH portal tabs
    RIMMINGTON_TELEPORT_TAB(HousePortal.RIMMINGTON, "Rimmington teleport", "Break"),
    TAVERLEY_TELEPORT_TAB(HousePortal.TAVERLY, "Taverley teleport", "Break"),
    RELLEKKA_TELEPORT_TAB(HousePortal.RELLEKKA, "Rellekka teleport", "Break"),
    BRIMHAVEN_TELEPORT_TAB(HousePortal.BRIMHAVEN, "Brimhaven teleport", "Break"),
    POLLNIVNEACH_TELEPORT_TAB(HousePortal.POLLNIVNEACH, "Pollnivneach teleport", "Break"),
    YANILLE_TELEPORT_TAB(HousePortal.YANILLE, "Yanille teleport", "Break"),
    HOSIDIUS_TELEPORT_TAB(HousePortal.HOSIDIUS, "Hosidius teleport", "Break"),
    PRIFDDINAS_TELEPORT_TAB(HousePortal.PRIFDDINAS, "Prifddinas teleport", "Break"),

    //POH portal tabs using "Teleport to house" tab (depending on where player POH is)
    POH_RIMMINGTON_TELEPORT_TAB(HousePortal.RIMMINGTON, "Teleport to house", "Outside", HousePortal.RIMMINGTON.isHome()),
    POH_TAVERLEY_TELEPORT_TAB(HousePortal.TAVERLY, "Teleport to house", "Outside", HousePortal.TAVERLY.isHome()),
    POH_POLLNIVNEACH_TELEPORT_TAB(HousePortal.POLLNIVNEACH, "Teleport to house", "Outside", HousePortal.POLLNIVNEACH.isHome()),
    POH_RELLEKKA_TELEPORT_TAB(HousePortal.RELLEKKA, "Teleport to house", "Outside", HousePortal.RELLEKKA.isHome()),
    POH_BRIMHAVEN_TELEPORT_TAB(HousePortal.BRIMHAVEN, "Teleport to house", "Outside", HousePortal.BRIMHAVEN.isHome()),
    POH_YANILLE_TELEPORT_TAB(HousePortal.YANILLE, "Teleport to house", "Outside", HousePortal.YANILLE.isHome()),
    POH_HOSIDIUS_TELEPORT_TAB(HousePortal.HOSIDIUS, "Teleport to house", "Outside", HousePortal.HOSIDIUS.isHome()),
    POH_PRIFF_TELEPORT_TAB(HousePortal.PRIFDDINAS, "Teleport to house", "Outside", HousePortal.PRIFDDINAS.isHome()),

    //Arceuus tabs
    SALVE_GRAVEYARD_TELEPORT_TAB(Spell.SALVE_GRAVEYARD_TELEPORT.getDestination(), "Salve Graveyard teleport", "Break", QuestRequirement.complete("PRIEST_IN_PERIL")),
    BARROWS_TELEPORT_TAB(Spell.BARROWS_TELEPORT.getDestination(), "Barrows teleport", "Break", QuestRequirement.complete("PRIEST_IN_PERIL")),
    WEST_ARDOUGNE_TELEPORT_TAB(Spell.WEST_ARDOUGNE_TELEPORT.getDestination(), "West ardougne teleport", "Break", QuestRequirement.complete("BIOHAZARD")),
    ARCEUUS_LIBRARY(Spell.ARCEUUS_LIBRARY_TELEPORT.getDestination(), "Arceuus library teleport", "Break"),
    DRAYNOR_MANOR(Spell.DRAYNOR_MANOR_TELEPORT.getDestination(), "Draynor manor teleport", "Break"),
    WEST_ARDOUGNE(Spell.WEST_ARDOUGNE_TELEPORT.getDestination(), "West ardougne teleport", "Break"),
    APE_ATOLL_TELEPORT(Spell.APE_ATOLL_TELEPORT.getDestination(), "Ape atoll teleport", "Break"),
    CEMETERY(Spell.CEMETERY_TELEPORT.getDestination(), "Cemetery teleport", "Break"),
    MIND_ALTAR_TELEPORT(Spell.MIND_ALTAR_TELEPORT.getDestination(), "Mind altar teleport", "Break"),
    FENKENSTRAINS_CASTLE(Spell.FENKENSTRAINS_CASTLE_TELEPORT.getDestination(), "Fenkenstrain's castle teleport", "Break"),
    FENKENSTRAINS_CASTLE_TELEPORT(Spell.FENKENSTRAINS_CASTLE_TELEPORT.getDestination(), "Fenkenstrain's castle teleport", "Break"),

    //Ancient Magicks tablets
    PADDEWWA_TELEPORT_TAB(Spell.PADDEWWA_TELEPORT.getDestination(), "Paddewwa teleport", "Break"),
    SENNTISTEN_TELEPORT_TAB(Spell.SENNTISTEN_TELEPORT.getDestination(), "Senntisten teleport", "Break"),
    KHARYRLL_TELEPORT_TAB(Spell.KHARYRLL_TELEPORT.getDestination(), "Kharyrll teleport", "Break"),
    LASSAR_TELEPORT_TAB(Spell.LASSAR_TELEPORT.getDestination(), "Lassar teleport", "Break"),
    DAREEYAK_TELEPORT_TAB(Spell.DAREEYAK_TELEPORT.getDestination(), "Dareeyak teleport", "Break"),
    CARRALLANGAR_TELEPORT_TAB(Spell.CARRALLANGAR_TELEPORT.getDestination(), "Carrallangar teleport", "Break"),
    ANNAKARL_TELEPORT_TAB(Spell.ANNAKARL_TELEPORT.getDestination(), "Annakarl teleport", "Break"),
    GHORROCK_TELEPORT_TAB(Spell.GHORROCK_TELEPORT.getDestination(), "Ghorrock teleport", "Break"),

    //Lunar tabs
    CATHERBY_TELEPORT_TAB(Spell.CATHERBY_TELEPORT.getDestination(), "Catherby teleport", "Break"),
    KHAZARD_TELEPORT_TAB(Spell.KHAZARD_TELEPORT.getDestination(), "Khazard teleport", "Break"),
    WATERBIRTH_TELEPORT_TAB(Spell.WATERBIRTH_TELEPORT.getDestination(), "Waterbirth teleport", "Break"),
    FISHING_GUILD_TELEPORT_TAB(Spell.FISHING_GUILD_TELEPORT.getDestination(), "Fishing guild teleport", "Break"),
    MOONCLAN_TELEPORT_TAB(Spell.MOONCLAN_TELEPORT.getDestination(), "Moonclan teleport", "Break"),
    OURANIA_TELEPORT_TAB(Spell.OURANIA_TELEPORT.getDestination(), "Ourania teleport", "Break"),
    ICE_PLATEAU_TELEPORT_TAB (Spell.ICE_PLATEAU_TELEPORT.getDestination(), "Ice plateau teleport", "Break"),
    BARBARIAN_TELEPORT_TAB(Spell.BARBARIAN_TELEPORT.getDestination(), "Barbarian teleport", "Break"),

    // Scrolls
    FELDIP_HILLS_TELEPORT(new Coordinate(2541, 2925, 0), "Feldip hills teleport", "Teleport"),
    PISCATORIS_TELEPORT(new Coordinate(2342, 3647, 0), "Piscatoris teleport", "Teleport"),
    MORTTON_TELEPORT(new Coordinate(3488, 3288, 0), "Mort'ton teleport", "Teleport"),
    DIGSITE_TELEPORT(new Coordinate(3324, 3412, 0), "Digsite teleport", "Teleport"),
    NARDAH_TELEPORT(new Coordinate(3420, 2917, 0), "Nardah teleport", "Teleport"),
    LUMBERYARD_TELEPORT(new Coordinate(3302, 3486, 0), "Lumberyard teleport", "Teleport"),
    TAI_BWO_TELEPORT(new Coordinate(2789, 3066, 0), "Tai bwo wannai teleport", "Teleport"),
    ZULANDRA_TELEPORT(new Coordinate(2197, 3055, 0), "Zul-andra teleport", "Teleport"),
    IORWERTH_TELEPORT(new Coordinate(2194, 3258, 0), "Iorwerth camp teleport", "Teleport"),

    // Items
    ECTOPHIAL(new Coordinate(3659, 3523, 0), "Ectophial", "Empty"),
    ROYAL_SEED_POD(new Coordinate(2465, 3495, 0), "Royal seed pod", "Commune", 30);

    private final Coordinate destination;
    private final String name;
    private final String action;
    private final int maxWildyDepth;
    private final Requirement additionalRequirements;

    InventoryItem(
        final Coordinate destination,
        final String name,
        final String action,
        final int maxWildyDepth,
        final Requirement additionalRequirements
    ) {
        this.destination = destination;
        this.name = name;
        this.action = action;
        this.maxWildyDepth = maxWildyDepth;
        this.additionalRequirements = additionalRequirements;
    }

    InventoryItem(HousePortal destination, String name, String action, Requirement... requirements) {
        this.destination = destination.getPosition();
        this.name = name;
        this.action = action;
        this.maxWildyDepth = 20;
        this.additionalRequirements = Arrays.stream(requirements).reduce(Requirements.none(), Requirement::and);
    }

    InventoryItem(final Coordinate destination, final String name, final String action) {
        this(destination, name, action, 20, null);
    }

    InventoryItem(final Coordinate destination, final String name, final String action, final int maxWildyDepth) {
        this(destination, name, action, maxWildyDepth, null);
    }

    InventoryItem(final Coordinate destination, final String name, final String action, final Requirement additionalRequirements) {
        this(destination, name, action, 20, additionalRequirements);
    }

    public static List<BasicItemTransport> transports() {
        return Arrays.stream(values()).map(teleport -> {
            final var name = Pattern.compile(teleport.getName());
            var requirement = Requirements.and(
                new ItemRequirement(name, 1),
                new WildernessRequirement(teleport.getMaxWildyDepth()),
                new TeleportsEnabled()
            );
            if (teleport.getAdditionalRequirements() != null) {
                requirement = requirement.and(teleport.getAdditionalRequirements());
            }
            return BasicItemTransport.builder()
                .itemName(name)
                .itemAction(Pattern.compile(teleport.getAction()))
                .destination(teleport.getDestination())
                .origin(BasicItemTransport.Origin.INVENTORY).requirement(requirement)
                .build();
        }).collect(Collectors.toList());
    }
}
