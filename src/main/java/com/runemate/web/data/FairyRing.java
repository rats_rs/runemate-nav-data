package com.runemate.web.data;

import static com.runemate.web.requirement.DiaryRequirement.Difficulty.*;
import static com.runemate.web.requirement.DiaryRequirement.Region.*;

import com.runemate.web.model.*;
import com.runemate.web.requirement.*;
import com.runemate.web.transport.fixed.*;
import com.runemate.web.util.*;
import java.util.*;
import java.util.regex.*;
import lombok.*;

@Getter
@RequiredArgsConstructor
public enum FairyRing {

    //We like these:
    BLR(new Coordinate(2740, 3351, 0), 1f), //Legend's guild
    DJP(new Coordinate(2658, 3230, 0), 1f), //Monastery

    AIQ(new Coordinate(2996, 3114, 0), 5f),
    AIR(new Coordinate(2700, 3247, 0), 5f),
    AJR(new Coordinate(2780, 3613, 0), 5f),
    AJS(new Coordinate(2500, 3896, 0), 5f),
    AKP(new Coordinate(3284, 2706, 0), 5f),
    AKQ(new Coordinate(2319, 3619, 0), 5f),
    AKS(new Coordinate(2571, 2956, 0), 5f),
    ALP(new Coordinate(2503, 3636, 0), 5f),
    ALQ(new Coordinate(3597, 3495, 0), 5f),
    ALS(new Coordinate(2644, 3495, 0), 5f),
    BIP(new Coordinate(3410, 3324, 0), 5f),
    BIQ(new Coordinate(3251, 3095, 0), 5f),
    BIS(new Coordinate(2635, 3266, 0), 5f),
    BJP(new Coordinate(2267, 2976, 0), 5f),
    BJS(new Coordinate(2150, 3070, 0), 5f),
    BKP(new Coordinate(2385, 3035, 0), 5f),
    BKR(new Coordinate(3469, 3431, 0), 5f),
    BLP(new Coordinate(2437, 5126, 0), 5f),
    CIP(new Coordinate(2513, 3884, 0), 5f),
    CIR(new Coordinate(1302, 3762, 0), 5f),
    CIS(new Coordinate(1638, 3868, 0), 5f),
    CJR(new Coordinate(2705, 3576, 0), 5f),
    CKR(new Coordinate(2801, 3003, 0), 5f),
    CKS(new Coordinate(3447, 3470, 0), 5f),
    CLP(new Coordinate(3082, 3206, 0), 5f),
    CLR(new Coordinate(2740, 2738, 0), 5f),
    CLS(new Coordinate(2682, 3081, 0), 5f),
    DIP(new Coordinate(3037, 4763, 0), 5f),
    DIS(new Coordinate(3108, 3149, 0), 5f),
    DJR(new Coordinate(1455, 3658, 0), 5f),
    DKP(new Coordinate(2900, 3111, 0), 5f),
    DKR(new Coordinate(3129, 3496, 0), 5f),
    DKS(new Coordinate(2744, 3719, 0), 5f),
    DLQ(new Coordinate(3423, 3016, 0), 5f),
    DLR(new Coordinate(2213, 3099, 0), 5f),
    BLS(new Coordinate(1295, 3493, 0), 5f),

    //Really don't like these:
//    CIQ(new Coordinate(2528, 3127, 0), 50f), //North of Yanille
    ;

    private final Coordinate position;
    private final float cost;

    private static final Pattern DRAMEN_STAFF = Pattern.compile("(?:Dramen|Lunar) staff");
    private static final Coordinate ZANARIS_TILE = new Coordinate(2412, 4434, 0);

    public static List<FairyRingTransport> transports() {
        final var results = new ArrayList<FairyRingTransport>();
        for (final var source : values()) {
            for (final var destination : values()) {
                if (source.equals(destination)) {
                    continue;
                }
                results.add(FairyRingTransport.builder()
                    .source(source.getPosition())
                    .destination(destination.getPosition())
                    .code(destination.name())
                    .cost(source.getCost())
                    .requirement(
                        new DiaryRequirement(LUMBRIDGE, ELITE)
                            .or(new VarbitRequirement(2326, 40, IntOp.GTE).and(new EquipmentRequirement(DRAMEN_STAFF))))
                    .build());
            }
        }
        return results;
    }

    public static List<GameObjectTransport> zanarisTransports() {
        final var results = new ArrayList<GameObjectTransport>();
        for (final var source: values()) {
            results.add(GameObjectTransport.builder()
                .source(source.getPosition())
                .destination(new Coordinate(2412, 4434, 0))
                .objectPosition(source.getPosition())
                .objectName("Fairy ring")
                .objectAction("Zanaris")
                .build());
        }
        return results;
    }
}
