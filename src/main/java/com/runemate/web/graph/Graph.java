package com.runemate.web.graph;

import com.runemate.web.edge.*;
import java.util.stream.*;
import lombok.*;
import org.jetbrains.annotations.*;


public /*sealed*/ interface Graph<V> /*permits GraphBase*/ extends AutoCloseable {

    @NotNull Stream<V> vertices();

    long vertexCount();

    boolean containsVertex(@NonNull V vertex);

    @NotNull Stream<Edge<V>> edges();

    @NotNull Stream<Edge<V>> edges(@NonNull V vertex);

    long edgeCount();

    boolean containsEdge(@NonNull Edge<V> edge);
}
