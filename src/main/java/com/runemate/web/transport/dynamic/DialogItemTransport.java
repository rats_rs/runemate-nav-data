package com.runemate.web.transport.dynamic;

import java.util.regex.*;
import lombok.*;
import lombok.experimental.*;

@Getter
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@SuperBuilder(toBuilder = true)
@ToString
public class DialogItemTransport extends BasicItemTransport {

    @NonNull Pattern optionPattern;

}
