package com.runemate.web.transport.dynamic;

import lombok.*;
import lombok.experimental.*;

@Getter
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@SuperBuilder(toBuilder = true)
@ToString
public class SpellTransport extends DynamicTransport {

    @NonNull String spell;
    @NonNull String spellbook;

}
