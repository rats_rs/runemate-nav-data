package com.runemate.web.util;

import java.util.*;
import java.util.stream.*;
import lombok.*;
import lombok.experimental.*;

@UtilityClass
public class Streams {

    public static <T> boolean equals(@NonNull Stream<T> expected, @NonNull Stream<?> actual) {
        Iterator<T> expectedIter = expected.iterator();
        Iterator<?> actualIter = actual.iterator();
        while (expectedIter.hasNext() && actualIter.hasNext()) {
            if (!Objects.equals(expectedIter.next(), actualIter.next())) {
                return false;
            }
        }
        return !expectedIter.hasNext() && !actualIter.hasNext();
    }

    public static <E> int hashCode(@NonNull Stream<E> stream) {
        int result = 1;
        Iterator<?> iterator = stream.iterator();
        while (iterator.hasNext()) {
            result = 31 * result + Objects.hashCode(iterator.next());
        }
        return result;
    }
}
