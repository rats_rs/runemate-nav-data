package com.runemate.web.util;

import com.runemate.web.edge.*;
import com.runemate.web.model.*;
import com.runemate.web.vertex.*;
import java.util.concurrent.*;
import lombok.*;
import lombok.experimental.*;

@UtilityClass
public class VertexUtil {

    private static final ConcurrentHashMap<Integer, VertexLiteral> VERTEX_LITERAL_CACHE = new ConcurrentHashMap<>();

    //plane_mapSquareX_mapSquareY_internalX_internalY
    private final Area VARROCK_DARK_CIRCLE = new Area(3216, 3361, 20, 20);
    private final Area BATTLEFIELD_LIZARD_MEN = new Area(1325, 3680, 50, 46);
    private final Area WHITE_WOLF_MOUTAIN = new Area(2835, 3463, 40, 80);
    private final Area WINTERTODT_WOLVES = new Area(1570, 3888, 20, 20);
    private final Area CATACOMBS_OF_KOUREND = new Area(1600, 9984, 127, 127);

    public boolean isInWilderness(@NonNull Edge<Vertex> edge) {
        return isInWilderness(edge.origin()) || isInWilderness(edge.destination());
    }

    public boolean isInWilderness(@NonNull Vertex vertex) {
        return getWildernessLevel(vertex) > 0;
    }

    /**
     * Ported from the logic provided in [proc,wilderness_level]()(int)
     * <p>
     * [proc,wilderness_level]()(int)
     * def_int $int0 = 0;
     * if (~inzone(0_52_62_0_0, 3_54_64_63_63, coord) = 1 & ~inzone(0_53_63_21_21, 3_53_63_42_42, coord) = 0) {
     * $int0 = 5;
     * } else if (~inzone(0_46_55_0_0, 3_52_67_63_63, coord) = 1) {
     * $int0 = calc((coordz(coord) - 55 * 64) / 8 + 1);
     * } else if (~inzone(0_47_158_0_0, 3_47_158_63_63, coord) = 1) {
     * $int0 = calc((coordz(coord) - 155 * 64) / 8 - 1);
     * } else if (~inzone(0_51_159_0_0, 3_51_159_63_63, coord) = 1) {
     * $int0 = 35;
     * } else if (~inzone(0_53_159_0_0, 3_53_159_63_63, coord) = 1) {
     * $int0 = 35;
     * } else if (~inzone(0_52_161_0_0, 3_52_161_63_63, coord) = 1) {
     * $int0 = 40;
     * } else if (~inzone(0_27_180_0_0, 3_27_180_63_63, coord) = 1) {
     * $int0 = 21;
     * } else if (~inzone(0_29_180_0_0, 3_29_180_63_63, coord) = 1) {
     * $int0 = 21;
     * } else if (~inzone(0_25_180_0_0, 3_25_180_63_63, coord) = 1) {
     * $int0 = 29;
     * } else if (~inzone(0_52_160_0_0, 3_52_160_63_63, coord) = 1) {
     * $int0 = calc(33 + scale(coordz(coord) % 64 - 6, 50, 7));
     * } else if (~inzone(0_46_155_0_0, 3_53_169_63_63, coord) = 1) {
     * $int0 = calc((coordz(coord) - 155 * 64) / 8 + 1);
     * }
     * return($int0);
     */
    public static int getWildernessLevel(Vertex coord) {
        int wildernessLevel = 0;
        if (areaContains(vertex(3328, 3968, 0), vertex(3503, 4163, 3), coord)
            //This first one looks like empty ocean in a square to the top right of the wilderness? Not sure what that's about
            && !areaContains(vertex(3413, 4053, 0), vertex(3434, 4074, 3), coord)) {
            //This is a non-wildy "safe area" that's 21x21 large. Appears to contain nothing but empty ocean. A safe spot in the random dangerous area.
            wildernessLevel = 5;
        } else if (areaContains(vertex(2944, 3520, 0), vertex(3391, 4307, 3), coord)) {
            //11830, 13380+ (x,y) [WILDY_GROUND_LEVEL_BOTTOM_LEFT to WILDY_GROUND_LEVEL_TOP_RIGHT]
            wildernessLevel = ((coord.y() - 3520) / 8) + 1;
        } else if (areaContains(vertex(3008, 10112, 0), vertex(3071, 10175, 3), coord)) {
            //12190
            wildernessLevel = ((coord.y() - 9920) / 8) - 1;
        } else if (areaContains(vertex(3264, 10176, 0), vertex(3327, 10239, 3), coord)) {
            //13215
            wildernessLevel = 35;
        } else if (areaContains(vertex(3392, 10176, 0), vertex(3455, 10239, 3), coord)) {
            //13727
            wildernessLevel = 35;
        } else if (areaContains(vertex(3328, 10304, 0), vertex(3391, 10367, 3), coord)) {
            //13473
            wildernessLevel = 40;
        } else if (areaContains(vertex(1728, 11520, 0), vertex(1791, 11583, 3), coord)) {
            // Wilderness Boss
            wildernessLevel = 21;
        } else if (areaContains(vertex(1856, 11520, 0), vertex(1919, 11583, 3), coord)) {
            // Wilderness Boss
            wildernessLevel = 21;
        } else if (areaContains(vertex(1600, 11520, 0), vertex(1663, 11583, 3), coord)) {
            // Wilderness Boss
            wildernessLevel = 29;
        } else if (areaContains(vertex(3328, 10240, 0), vertex(3391, 10303, 3), coord)) {
            //13472
            wildernessLevel = ((7 * ((coord.y() % 64) - 6)) / 50) + 33;
        } else if (areaContains(vertex(2944, 9920, 0), vertex(3455, 10815, 3), coord)) {
            //WILDY_UNDERGROUND_BOTTOM_LEFT -> WILDY_UNDERGROUND_RIGHT_EDGE (Includes the underground sections such as the wilderness slayer cave, revs, etc) and derives everything using the standard wildy algorithm
            wildernessLevel = ((coord.y() - 9920) / 8) + 1;
        }
        return wildernessLevel;
    }

    public static int getWildernessLevel(Edge<Vertex> edge) {
        return Math.max(getWildernessLevel(edge.origin()), getWildernessLevel(edge.destination()));
    }

    public static boolean areaContains(Vertex minimum, Vertex maximum, Vertex current) {
        if ((current.x() < minimum.x()) || (current.x() > maximum.x())) {
            return false;
        }
        if ((current.y() < minimum.y()) || (current.y() > maximum.y())) {
            return false;
        }
        return (current.z() >= minimum.z()) && (current.z() <= maximum.z());
    }

    public static float calculateCostBetween(Vertex source, Vertex target) {
        float cost = (float) distance(source, target);
        int wildernessLevel = getWildernessLevel(target);
        if (wildernessLevel > 0) {
            cost *= 3;
            cost += wildernessLevel * 0.4;
        }
        if (containsEither(VARROCK_DARK_CIRCLE, source, target)
            || containsEither(BATTLEFIELD_LIZARD_MEN, source, target)
            || containsEither(WHITE_WOLF_MOUTAIN, source, target)
            || containsEither(WINTERTODT_WOLVES, source, target)
            || containsEither(CATACOMBS_OF_KOUREND, source, target)) {
            cost *= 10;
        }
        return cost;
    }

    private boolean containsEither(Area area, Vertex source, Vertex target) {
        return area.contains(source) || area.contains(target);
    }

    public static double distance(Vertex first, Vertex second) {
        return calculate(first.x(), first.y(), second.x(), second.y());
    }

    public static double calculate(long startX, long startY, long destinationX, long destinationY) {
        if ((startX == destinationX) && (startY == destinationY)) {
            return 0;
        }
        return Math.addExact(
            Math.abs(Math.subtractExact(destinationX, startX)),
            Math.abs(Math.subtractExact(destinationY, startY))
        );
    }

    public static VertexLiteral vertex(int x, int y, int z) {
        int hash = z << 28 | y << 14 | x;
        if (VERTEX_LITERAL_CACHE.containsKey(hash)) {
            return VERTEX_LITERAL_CACHE.get(hash);
        }
        VertexLiteral vertex = new VertexLiteral(x, y, z);
        VERTEX_LITERAL_CACHE.put(hash, vertex);
        return vertex;
    }
}
